# 🧭 Accueil

Bienvenue sur le wiki ENSIBFS Club mousaillon.

Tu trouveras ici un bon nombre d'informations pratiques sur les formation ENSIBS, les campus, et la vie étudiante en général.
Envie de contribuer ? La documentation est sous [licence CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr) et toutes les informations pour participer sont disponibles sur le [dépôt gitlab](https://gitlab.com/Sykursen/ensibs-wiki).
