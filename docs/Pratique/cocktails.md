# 🍹 Cocktails

En période de canicule il est fortement recommandé de s'hydrater régulièrement, c'est pourquoi nous vous proposons une liste de breuvages pour lutter contre les grandes chaleurs.

## Moscow mule

| Ingredient         | Dosage | Commentaire |
|--------------------|--------|-------------|
| Vodka              | 4 cl   |             |
| Jus de citron vert | 1.5 cl |             |
| Ginger beer        | 12 cl  |             |
| Glaçons            |        |             |

## Madeleine

| Ingrédient      | Dosage       | Commentaire                                           |
|-----------------|--------------|-------------------------------------------------------|
| Triple sec      | 1 dose       | Diminuer au besoin                                    |
| Liqueur de café | 1 dose       | Ne pas en abuser ça casse le goût                     |
| Amareto         | 1 dose       |                                                       |
| Cointreau       | 1 - 1.5 dose | En fonction du goût voulu                             |
| Jus d'ananas    | 4 - 7 doses  | À ajuster au nez pour obtenir l'odeur de la madeleine |

