# 🔐 Culture Cybersécurité

Voici un regroupement d'informations pour mieux intégrer l'univers de la cybersécurité

## 🚩 CTF

On en parle très souvent mais qu'est-ce qu'un CTF ?
Un CTF ou capture de flag est une compétition dans lequel vous devez résoudre des challenges pour obtenir un drapeau. La plupart du temps cela ressemble à ça : `CTF{ZeSuisUnH@cker}`. Une fois obtenu les participants doivent valider leurs drapeaux aux organisateurs et ainsi obtenir des points. Ces points permettent un classement des équipes ou des joueurs.

Certains CTF sont uniquement par équipe, ou bien en solitaire. Attention les partage de drapeau entre équipes est très mal vue (Shareflag) et souvent pénalisable.

Certains CTF sont spécialisés dans un type de challenge (OSINT, Prog, Crypto, etc) ou alors en comporte plusieurs catégories. Le mode de jeu le plus connu est le géopardie qui regroupe plusieurs types de challenges.

Il existe aussi le mode attack-defend qui oppose plusieurs équipes devant chacune se défendre et attaquer les autres.

## 💡 Types de challenges

- **Web** Challenges autour des vulnérabilités web (Injections, API, HTTP)
- **Prog** Challenges autour de résolution par programmation
- **Crypto** Challenges autour des notions de cryptographie (GPG, HTTPS, )
- **Pawn ou pwn** Challenges autour de la pratique de dépassement de tampons dans des binaires non protégés
- **Reverse** Challenges autour de (Assembleur, Hexa)
- **System** Challenges autour de l'administration système
- **Kernel** Challenges autour des vulnérabilités Kernels
- **Stegano** Challenges autour de technique de cryptographie (Audio, Images, Fichiers)
- **Forensic** Challenges autour d'analyses forensics après intrusions
- **Box** Challenges autour de serveur à infiltrer
- **OSINT** Challenges autour du renseignement par source ouverte (Traces GPS, Maps, Cache)
- **AI** Challenges autour des intelligences artificielles
- **Blockchain** Challenges autour des blochchaines (Ethereum, Bitcoin)
- **Misc** Tous les autres challenges qui ne rentrent pas dans les catégories précédentes

## 💼 Métiers

Il existe de nombreux métiers dans la cybersécurité. En voici une liste non exhaustive.

- **Analyste SOC** Analyse et monitore les assets de l'entreprise pour prévenir d'intrusion et réaliser les actions de remédiation.
- **Architecte sécurité** Encadre l'architecture des services informatiques et conseil les équipes.
- **Pentester** Réaliser des tests d'intrusions autorisé dans des SI, applications web, mobile, etc.
- **Programmeur sécurité** Programmeur spécialisé dans la sécurisation des applications.

Selon la taille des entreprises, chaque poste peut regrouper des missions provenant de plusieurs types de métiers.

## Certifications Cybersécurité 📜

> ToDo

- **OSCP**

## Certifications Entreprise 📜

- **ISO 27001** Certification affirmant que l'entreprise possède des processus de management de la sécurité de l'information (SMSI)
