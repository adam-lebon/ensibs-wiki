# 🏘️ Logements

Voici quelques astuces pour trouver un logement.
Une majorité d'étudiants possèdent deux logements. Un sur Vannes pour l'école et un autre à proximité de leur lieu de travail.

Pour vous aider dans vos recherches de logement, le [site du bureau jeunesse de Vannes](http://www.bij-vannes.fr/ma-vie-a-vannes/le-logement/) propose plusieurs pistes.

## Appart-City 🏨

Envie de louer un appartement sur Vannes seulement pendant vos semaines de cours ? Et oui c'est possible grâce à l'appart city. Pour environ **450€** en demandant la tarification étudiante pour une période scolaire. Vous pourrez ainsi bénéficier d'un logement à 5 minutes à pied des locaux l'ENSIBS.

Toutes les informations en détails [ici](https://www.appartcity.com/fr/offre-etudiante.html).

## Crous 🏠

> ToDo
