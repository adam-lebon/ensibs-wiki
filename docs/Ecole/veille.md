# 📰 Veille technologique

Durant votre S5 et votre S6 vous serez amenés à réaliser un état de l'art sur un sujet de votre choix. Voici une liste de ressources pour vous aider dans ce long périple.

> Attention, lors du jalon 3 il sera demandé un rendu écrit en **latex**. Le passage d'un document word à un document latex fait la plupart du temps rétrécir la taille de votre document.

## Portails de recherche UBS 🔎

Voici une liste des ressources mises à disposition par l'UBS.

- [Menu des portails](https://ezproxy.univ-ubs.fr/menu)
- [Google Scholar](https://googlescholar.ezproxy.univ-ubs.fr/)
- [IEEE](https://ieee.ezproxy.univ-ubs.fr/Xplore/home.jsp)
- [Technique de l'ingénieur](https://techniquesdelingenieur.ezproxy.univ-ubs.fr/)

## Autres sources📚 

- [FreeFullPDF](https://www.freefullpdf.com/)
- [Archives Ouvertes](https://hal.archives-ouvertes.fr/)
