# 🎭 Associations et clubs

## BDE 🍻

Que serait une école sans un BDE franchement ? Pas grand chose. Le BDE de l'ENSIBS couvre les sites de Vannes et Lorient. Il s'occupe de l'accueil et de la vie étudiante sur le campus. Le BDE s'occupe d'organiser le WEC de bienvenue et propose également une semaine au ski chaque hiver.

Géré par `@Bananarazzi#4969`, `@Alexis.L#4977`, `@Camillou#8714`, `@Terax#4359`, `@Francis#7450`, `@guyguy#2644`, `@Inès#5876`, `@Lucky#9612`, `@Lucie#6383`, `@Mitsuko#7599`,`@Prantice#2587`, `@guyguy#2644` et `@Niels#2778`

- [Discord](assos-clubs.md)
- [Instagram](https://www.instagram.com/bde_ensibs/)
- [Mail](bde.vannes.ensibs|@|gmail.com)

## Hack2G2 📚

Hack2g2 est une association créée par les étudiants de l'IUT de Vannes et de l'ENSIBS pour promouvoir le partage des connaissances majoritairement informatiques. Événements, conférences, lives et ateliers sont organisés par l'association pour les membres. Chaque année, l'association organise la hitchhack, une conférence de plusieurs jours avec de nombreux intervenants.

Géré par `@MasterFox#5806`, `@Louarnig#3043`, `@FKN_Apollo#6517`, `@Sykursen#4002`, `@SoEasY#4115` et `@!xanhacks#7312`.

- [Discord](https://discord.gg/RSJJqE48Jw)
- [Site Web](https://hack2g2.fr/)
- [Twitter](https://twitter.com/Hack2G2)
- [Peertube](https://videos.hack2g2.fr/)

## Club CTF - APT56 🚩

Envie de soulever tous les CTF de France et de Navarre jusqu'au FCSC ? Alors bienvenue au club CTF.

- [Discord](https://discord.gg/ukHMpeJ6h6)

Géré par `@empiresailor#0546` et `@Adam Le Boo#6357`.

## ENSIBF Holdings 💰

Vous souhaitez parler actions, cryptomonnaies et grosse moula ? Bienvenue chez ENSIBF Holding, le club des petits investisseurs rusés qui partagent leurs astuces et savoir faire.

- [Discord](https://discord.gg/Bnb84dJeGd)

Géré par `@CaptainDartz | Thomas#7270`, `@empiresailor#0546` et `@Lucky#9612`.

## Airsoft 🔫

> ToDo
